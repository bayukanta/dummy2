from django.test import TestCase
from django.http import HttpRequest
from .views import helloworld

class HelloWorldTest(TestCase):
	def test_donation_url_exist(self):
		response = helloworld(HttpRequest())
		html_response = response.content.decode('utf8')
		self.assertIn("Hello World",html_response)
